'use strict';

class Cookie 
{
    constructor(name, value='', domain='', expires=0, secure=false, path='/', httpOnly=false) {
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.expires = expires;
        this.secure = secure;
        this.path = path;
        this.httpOnly = httpOnly;
    }

    static getCookies(req) {
        const cookieObj = {};
        const cookieStr = req.headers.cookie || '';
        const cookies = cookieStr ? cookieStr.split(';') : [];
        let len = cookies.length;
        let cookie;

        if (!cookies) {
            return cookieObj;
        }
        while (len--) {
            cookie = cookies[len].split('=');
            let name = decodeURIComponent(cookie[0]).replace(/\s/g, '');
            let value = decodeURIComponent(cookie[1]);
            cookieObj[name] = new Cookie(name, value);
        }
        return cookieObj;
    }

    set(res) {
        let { name, value, domain, secure, expires, path, httpOnly } = this;
        if (name) {
            let cookie = [];
            cookie.push(`${encodeURIComponent(name)}=${encodeURIComponent(value)}`);
            cookie.push(`path=${path || '/'}`);
            if (domain) cookie.push(`domain=${domain}`);
            if (secure) cookie.push('secure');
            if (httpOnly) cookie.push('httpOnly');
            if (expires) {
                // using "expires" because IE doesn't support "max-age"
                expires = new Date(new Date().getTime() +
                    parseInt(expires, 10) * 1000 * 60 * 60 * 24);
                // use toUTCstring method to convert expires date to a string:
                cookie.push(`expires=${expires.toUTCString()}`);
            }
            //console.log('Set-Cookie:', cookie.join('; '));
            res.setHeader('Set-Cookie', cookie.join('; '));
            return true;
        }

        return false;
    }

    remove(res) {
        return this.set(res, {
            name: this.name,
            value: '',
            expires: -1,
            path: this.path,
            domain: this.domain
        });
    }
}


module.exports.Cookie = Cookie;
